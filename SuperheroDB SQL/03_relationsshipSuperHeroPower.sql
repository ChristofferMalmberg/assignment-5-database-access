
DROP TABLE IF EXISTS hero_power;

CREATE TABLE hero_power(
    hero_id int REFERENCES superhero,
    power_id int REFERENCES power,
    PRIMARY KEY (hero_id, power_id)
)