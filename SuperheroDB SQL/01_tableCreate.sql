DROP TABLE IF EXISTS Power;

CREATE TABLE Power (
    power_id serial PRIMARY KEY,
    name varchar(30) NOT NULL,
    description varchar(255) NOT NULL
);

DROP TABLE IF EXISTS Superhero;

CREATE TABLE Superhero (
    hero_id serial PRIMARY KEY,
    name varchar(30) NOT NULL,
    alias varchar(50) NOT NULL,
    origin varchar(50) NOT NULL
);

DROP TABLE IF EXISTS Assistant;

CREATE TABLE Assistant (
    assist_id serial PRIMARY KEY,
    name varchar(30) NOT NULL
);

