INSERT INTO power (name, description) VALUES ('Super-Strength', 'Makes you super strong');
INSERT INTO power (name, description) VALUES ('Mega-Intelligence', 'Super smart');
INSERT INTO power (name, description) VALUES ('Rich', 'All your problems disappear');
INSERT INTO power (name, description) VALUES ('Tech', 'High tech equipment');


INSERT INTO hero_power (hero_id, power_id) VALUES (1,1);
INSERT INTO hero_power (hero_id, power_id) VALUES (1,3);
INSERT INTO hero_power (hero_id, power_id) VALUES (1,4);
INSERT INTO hero_power (hero_id, power_id) VALUES (3,3);
INSERT INTO hero_power (hero_id, power_id) VALUES (3,4);
INSERT INTO hero_power (hero_id, power_id) VALUES (2,1);

