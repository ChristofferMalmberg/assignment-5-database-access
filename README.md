# Springboot Database Assignment 5
A small application to query information from a customer database.
For this application the chinook database is setup in postgres, and the SQL script for setup is 
provided in this repo.
The application uses SpringBoot and Postgres and is setup using the repository pattern.
In the application it is possible to do several different queries for example
getting all customers, specific customers by id and their favourite genres.
Since this application is following the concept of CRUD there is also features for
inserting, updating and deleting customers.

## Requirements
The application can run locally with the following requirements.
- JDK 17
- Gradle
- Postgres
- Chinook Database
## Install
- Clone project
- Setup chinook database by creating a database using the chinook_pg_serial_pk_proper_naming.sql script
to generate tables and data
- Open project with IntelliJ or other preferred IDE
- Open application.properties file
- Set the values for connecting to your database (url, username, password) example below
```
    spring.datasource.url= <Your DB URL>
    spring.datasource.username= <Your DB USERNAME>
    spring.datasource.password= <Your DB PASSWORD>
```

## Usage
A few useful method from the application (CRUD operations in the application).
Run in the CustomerRunner Class or create your own AppRunner Class.
```
        //Find all customers in the database
        customerRepository.findAll();

        //Finds the specific customer which has an id of 1
        customerRepository.findById(1);

        //Inserts a customer into the database
        customerRepository.insert(customer)

        //Deletes the specific customer which has the id of 89
        customerRepository.deleteById(89)

        //Updates the input customer 
        customerRepository.update(customerUpdate)
```

## Contributing
- @Maltin1234
- @ChristofferMalmberg 
