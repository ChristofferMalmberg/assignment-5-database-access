package com.example.databaseaccess.repositories;

import java.util.List;

public interface CrudRepository <T, ID>{
    /**
     * Retrieves a list of objects from the database
     * @return a List of the desired objects where T is the type parameter
     */
    List<T> findAll();
    /**
     * Retrieves a limited list of objects from the database skipping the number provided by offset
     * @param limit Amount to be retrieved
     * @param offset Amount to be skipped
     * @return a List of objects of type T where T is the type parameter
     */
    List<T> findAll(int limit, int offset);

    /**
     * Retrieves an object by its provided ID
     * @param id an id for the object, where ID is a generic
     * @return Returns an object T where T is the type parameter
     */
    T findById(ID id);

    /**
     * Inserts the object into the database
     * @param entity an object of type T Where T is the type parameter
     * @return number of rows affected by the insert
     */
    int insert(T entity);

    /**
     * Updates an object in the database
     * @param entity the object of type T to be updated
     * @return number of rows affected by the update
     */
    int update(T entity);

    /**
     * Deletes an object from the database
     * @param entity an object of type T where T is the type parameter
     * @return number of rows affected by the deletion
     */
    int delete(T entity);

    /**
     * Deletes an object from the database based on the id of the object
     * @param id an id of type ID where ID is the type parameter
     * @return
     */
    int deleteById(ID id);

}
