package com.example.databaseaccess.repositories.customer;

import com.example.databaseaccess.models.customer.Customer;
import com.example.databaseaccess.models.customer.CustomerCountry;
import com.example.databaseaccess.models.customer.CustomerGenre;
import com.example.databaseaccess.models.customer.CustomerSpender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {

    private final String url;
    private final String username;
    private final String password;

    CustomerRepositoryImpl(@Value("${spring.datasource.url}") String url,
                           @Value("${spring.datasource.username}") String username,
                           @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    //Finds all customers in the database
    //Returns a list of all the customers
    @Override
    public List<Customer> findAll() {
        List<Customer> customerList = new ArrayList<>();
        Customer customer = null;
        String sql = "SELECT * FROM customer ORDER BY customer_id";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
                customerList.add(customer);
            }
        } catch (SQLException e) {
            System.err.println("Exception in findAll: " + e.getMessage());
        }
        return customerList;
    }

    //Finds all customers within the set limit starting at the index
    //after the offset value
    //Returns a list of the customers
    @Override
    public List<Customer> findAll(int limit, int offset) {
        List<Customer> customerList = new ArrayList<>();
        Customer customer = null;
        String sql = "SELECT * FROM customer ORDER BY customer_id LIMIT ? OFFSET ?";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, limit);
            preparedStatement.setInt(2, offset);
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
                customerList.add(customer);
            }
        } catch (SQLException e) {
            System.err.println("Exception in findAll(limit, offset): " + e.getMessage());
        }
        return customerList;
    }

    //Find a specific customer by their id
    //Returns the customer object
    @Override
    public Customer findById(Integer id) {
        Customer customer = null;
        String sql = "SELECT * FROM customer WHERE customer_id = ?";

        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
            }

        } catch (SQLException e) {
            System.err.println("Exception in findById: " + e.getMessage());
        }
        return customer;
    }


    //Finds a list of customer by their names
    //returns a list of all the found customers
    @Override
    public List<Customer> findByName(String name) {
        Customer customer = null;
        List<Customer> customerList = new ArrayList<>();
        String sql = "SELECT * FROM customer WHERE first_name LIKE ?";

        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, name + '%');
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customer = new Customer(
                        resultSet.getInt("customer_id"),
                        resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getString("country"),
                        resultSet.getString("postal_code"),
                        resultSet.getString("phone"),
                        resultSet.getString("email")
                );
                customerList.add(customer);
            }

        } catch (SQLException e) {
            System.err.println("Exception in findByName: " + e.getMessage());
        }
        return customerList;
    }

    //Takes a customer and inserts it into the database
    //Returns rows affected

    /**
     * waaawaaaweewaa
     * @param customer an object of type T Where T is the type parameter
     * @return
     */
    @Override
    public int insert(Customer customer) {
        String sql = "INSERT INTO customer (first_name, last_name, country, postal_code, phone, email) " +
                "VALUES (?,?,?,?,?,?) ";
        int rowsAffected = 0;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLast_name());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setString(4, customer.getPostal_code());
            preparedStatement.setString(5, customer.getPhone());
            preparedStatement.setString(6, customer.getEmail());
            rowsAffected = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Exception in insert: " + e.getMessage());
        }
        return rowsAffected;
    }

    //Takes a customer as input and updates that customer in the database
    //Returns rows affected
    @Override
    public int update(Customer customer) {
        String sql = "UPDATE customer SET first_name = ?," +
                "last_name = ?, " +
                "country = ?, " +
                "postal_code = ?, " +
                "phone = ?, " +
                "email = ?" +
                "WHERE customer_id = ?";
        int rowsAffected = 0;

        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setString(1, customer.getFirstName());
            preparedStatement.setString(2, customer.getLast_name());
            preparedStatement.setString(3, customer.getCountry());
            preparedStatement.setString(4, customer.getPostal_code());
            preparedStatement.setString(5, customer.getPhone());
            preparedStatement.setString(6, customer.getEmail());
            preparedStatement.setInt(7, customer.getId());
            rowsAffected = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.println("Exception in update: " + e.getMessage());
        }
        return rowsAffected;
    }

    //find and deletes a customer with customer object as input
    //Returns rows affected
    @Override
    public int delete(Customer customer) {
        String sql = "DELETE FROM customer WHERE customer_id = ?";
        int rowsAffected = 0;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, customer.getId());
            rowsAffected = preparedStatement.executeUpdate();

        } catch (SQLException e) {
            System.err.println("Exception in delete: " + e.getMessage());
        }
        return rowsAffected;
    }

    //Finds and deletes a customer by their id
    //Returns rows affected
    @Override
    public int deleteById(Integer id) {
        String sql = "DELETE FROM customer WHERE customer_id = ?";
        int rowsAffected = 0;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            rowsAffected = preparedStatement.executeUpdate();
        } catch (SQLException e) {
            System.err.println("Exception in deleteById: " + e.getMessage());
        }
        return rowsAffected;
    }

    //Retrieves the country which has the highest amount of customers
    //Returns the customerCountry
    @Override
    public CustomerCountry maxCountries() {
        String sql = "SELECT MAX(country) as maxCountry FROM customer";
        CustomerCountry customerCountry = null;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customerCountry = new CustomerCountry(resultSet.getString("maxCountry"));

            }

        } catch (SQLException e) {
            System.err.println("Exception in maxCountries: " + e.getMessage());
        }
        return customerCountry;
    }

    //Retrieves the customer who has spent the most amount of money in the store
    //Returns the customer
    @Override
    public CustomerSpender highestSpender() {
        String sql =
                "SELECT SUM(total) as tot, first_name, last_name FROM invoice\n" +
                        "INNER JOIN customer ON customer.customer_id = invoice.customer_id\n" +
                        "GROUP BY customer.customer_id ORDER BY tot DESC LIMIT 1\n";
        CustomerSpender customerSpender = null;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                customerSpender = new CustomerSpender(resultSet.getString("first_name"),
                        resultSet.getString("last_name"),
                        resultSet.getDouble("tot"));
            }

        } catch (SQLException e) {
            System.err.println("Exception in highestSpender: " + e.getMessage());
        }
        return customerSpender;
    }


    //Retrieves the most purchased genre by a customer, and in case of ties returns all the
    //tied genres
    //Returns a list of customerGenre
    @Override
    public CustomerGenre favoriteGenre(Integer id) {
        CustomerGenre customerGenre = null;
        List<String> genreList = new ArrayList<>();
        String sql = "SELECT genre.name FROM invoice " +
                "INNER JOIN invoice_line ON invoice.invoice_id = invoice_line.invoice_id " +
                "INNER JOIN track ON invoice_line.track_id = track.track_id " +
                "INNER JOIN genre ON track.genre_id = genre.genre_id WHERE invoice.customer_id = ? " +
                "GROUP BY genre.name ORDER BY COUNT(genre.genre_id) DESC FETCH NEXT 1 ROWS WITH TIES";
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement preparedStatement = conn.prepareStatement(sql);
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                genreList.add(resultSet.getString("name"));
            }
            customerGenre = new CustomerGenre(genreList);
        } catch (SQLException e) {
            System.err.println("Exception in favoriteGenre: " + e.getMessage());
        }
        return customerGenre;
    }
}