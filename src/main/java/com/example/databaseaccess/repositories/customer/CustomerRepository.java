package com.example.databaseaccess.repositories.customer;

import com.example.databaseaccess.models.customer.Customer;
import com.example.databaseaccess.models.customer.CustomerCountry;
import com.example.databaseaccess.models.customer.CustomerGenre;
import com.example.databaseaccess.models.customer.CustomerSpender;
import com.example.databaseaccess.repositories.CrudRepository;

import java.util.List;

public interface CustomerRepository extends CrudRepository <Customer, Integer> {

    /**
     * Retrieves customers by name, gets all the customer with a specific name
     * in case of identical names
     * @param name String "name" of the customer to be searched for
     * @return a list of Customer objects
     */
    List<Customer> findByName(String name);
    //Gets the country containing the most customers
    public CustomerCountry maxCountries();

    /**
     * Retrieves the customerCountry which has the highest amount of customers
     * @return customerCountry object
     */
    public CustomerSpender highestSpender();

    /**
     * Retrieves a customers most purchased genre and in case of ties
     * gets all the tied ones.
     * @param id the int "id" of the customer
     * @return returns a list of customerGenre objects with the top/ties
     */
    public CustomerGenre favoriteGenre(Integer id);

}
