package com.example.databaseaccess.models.customer;

public record CustomerSpender(String firstName, String lastName, double spent) {


}
