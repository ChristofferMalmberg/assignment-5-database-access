package com.example.databaseaccess.models.customer;

import java.util.List;

public record CustomerGenre(List<String> genreTitle) {
}
