package com.example.databaseaccess.models.customer;

public class Customer {
    private int id = 0;
    private String firstName;
    private String last_name;
    private String country;
    private String postal_code;
    private String phone;
    private String email;

    //Used when inserting into database as the database handles id's by auto increment
    public Customer(String firstName, String last_name, String country, String postal_code, String phone, String email) {
        this.firstName = firstName;
        this.last_name = last_name;
        this.country = country;
        this.postal_code = postal_code;
        this.phone = phone;
        this.email = email;
    }

    //Used when reading from the database
    public Customer(int id, String firstName, String last_name, String country, String postal_code, String phone, String email) {
        this.id = id;
        this.firstName = firstName;
        this.last_name = last_name;
        this.country = country;
        this.postal_code = postal_code;
        this.phone = phone;
        this.email = email;
    }

    public int getId(){
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
