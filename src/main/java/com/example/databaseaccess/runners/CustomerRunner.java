package com.example.databaseaccess.runners;


import com.example.databaseaccess.models.customer.Customer;
import com.example.databaseaccess.repositories.customer.CustomerRepository;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;


@Component
public class CustomerRunner implements ApplicationRunner {

    private final CustomerRepository customerRepository;

    public CustomerRunner(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void run(ApplicationArguments args) {
        //Find All test
        List<Customer> customerList;
        customerList = customerRepository.findAll();
        System.out.println("List size " + customerList.size());
        System.out.println("10 first customers in list: ");
        for(int i = 0; i < 10; i++){
            System.out.println(customerList.get(i).getFirstName());
        }

        //FindAll offset and limit
        List<Customer> customerList2;
        customerList2 = customerRepository.findAll(10,20);
        System.out.println("List size " + customerList2.size());
        for(int i = 0; i < customerList2.size(); i++){
            System.out.println(customerList2.get(i).getId() + " " + customerList2.get(i).getFirstName());
        }

        //Find by id
        Customer customer = customerRepository.findById(1);
        System.out.println("ID: " + customer.getId() + " " + customer.getFirstName());

        //Find by name
        List<Customer> customerByNameList = customerRepository.findByName("D");
        for(int i = 0; i < customerByNameList.size(); i++){
            System.out.println(customerByNameList.get(i).getId() + " " + customerByNameList.get(i).getFirstName() + " " + customerByNameList.get(i).getLast_name());
        }

        //Insert test
        Customer customerIn = new Customer("Stefan", "Stefansson", "Sweden", "12345", "00099123", "S@Stefan.com");
        System.out.println("Rows affected: " + customerRepository.insert(customerIn));
        //Get all and check if customer is inserted by checking array new size
        customerList = customerRepository.findAll();
        customerRepository.findByName("name");
        System.out.println("List size " + customerList.size());

        //Update test
        Customer customerUpdate = customerRepository.findById(1);
        System.out.println(customerUpdate.getFirstName());
        customerUpdate.setFirstName("Johan");
        customerRepository.update(customerUpdate);
        customerUpdate = customerRepository.findById(1);
        System.out.println(customerUpdate.getFirstName());

        //Delete Customer (Assumption for assignment is that customer does not have invoices)
        Customer customerDelete = new Customer ("Testy", "Tester", "Sweden", "12345", "00099123", "T@Testy.com");
        customerRepository.insert(customerDelete); //Insert to test delete
        customerDelete = customerRepository.findByName("Testy").get(0); //
        customerRepository.delete(customerDelete);

        //Delete Customer by ID (Assumption for assignment is that customer does not have invoices)
        System.out.println(customerRepository.deleteById(89)); //(Set own id to a customer without invoice)

        //Get country with most customers
        System.out.println(customerRepository.maxCountries().name());

        //Get the customer who spent the most money
        System.out.println(customerRepository.highestSpender().firstName());

        //Get a customers favorite genre
        System.out.println("Customer id = 1 favorite genre: " +  Arrays.toString(customerRepository.favoriteGenre(1).genreTitle().toArray()));
    }
}
